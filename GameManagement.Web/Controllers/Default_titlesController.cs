﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GameManagement.Models;

namespace GameManagement.Controllers
{
    public class Default_titlesController : ApiController
    {
        private GameMangementEntities db = new GameMangementEntities();

        // GET: api/Default_title
        public IQueryable<dynamic> GetDefault_titles()
        {
            var query = from dt in db.Default_title
                        join t in db.Titles on dt.Title_ID equals t.Id
                        join p in db.PayRates on t.PayCode equals p.PayCode
                        select new
                        {
                            dt.Id,
                            dt.Staff_ID,
                            dt.Sport_ID,
                            dt.Title_ID,
                            dt.Position,
                            t.Rhours,
                            t.Report,
                            p.RegCalc
                        };

            return query;
        }

        #region Other actions
        /*
        // GET: api/Default_title/5
        [ResponseType(typeof(Default_title))]
        public async Task<IHttpActionResult> GetDefault_title(int id)
        {
            Default_title default_title = await db.Default_title.FindAsync(id);
            if (default_title == null)
            {
                return NotFound();
            }

            return Ok(default_title);
        }

        // PUT: api/Default_title/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDefault_title(int id, Default_title default_title)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != default_title.Id)
            {
                return BadRequest();
            }

            db.Entry(default_title).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Default_titleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Default_title
        [ResponseType(typeof(Default_title))]
        public async Task<IHttpActionResult> PostDefault_title(Default_title default_title)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Default_title.Add(default_title);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = default_title.Id }, default_title);
        }

        // DELETE: api/Default_title/5
        [ResponseType(typeof(Default_title))]
        public async Task<IHttpActionResult> DeleteDefault_title(int id)
        {
            Default_title default_title = await db.Default_title.FindAsync(id);
            if (default_title == null)
            {
                return NotFound();
            }

            db.Default_title.Remove(default_title);
            await db.SaveChangesAsync();

            return Ok(default_title);
        }
        */
        #endregion


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Default_titleExists(int id)
        {
            return db.Default_title.Count(e => e.Id == id) > 0;
        }
    }
}