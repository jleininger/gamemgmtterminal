﻿using GameManagement.Models;
using GameManagement.Services;
using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using GameManagement.Models.ViewModels;

namespace GameManagement.Controllers
{
    public class DetailSheetsController : ApiController
    {
        private GameMangementEntities db = new GameMangementEntities();
         private string pouchDbUrl = "http://164.67.162.36:5984";
        //private string pouchDbUrl = "http://localhost:5984";

        [Route("api/detailsheets/generate"), HttpGet]
        public async Task<IHttpActionResult> GenerateDetailSheet(string eventId)
        {
            var query = from assign in db.Assigns
                        where assign.Event_ID == eventId
                        join title in db.Titles
                            on assign.Title equals title.Id
                        join eventx in db.Events
                            on assign.Event_ID equals eventx.Id
                        join staff in db.Staffs
                            on assign.Staff_ID equals staff.Id
                        join payrate in db.PayRates
                            on title.PayCode equals payrate.PayCode
                        orderby staff.Lname, staff.Fname
                        select new
                        {
                            id = assign.Id,
                            staff_ID = assign.Staff_ID,
                            gross = assign.Gross,
                            lname = staff.Lname,
                            fname = staff.Fname,
                            timeIn = assign.TimeIn,
                            timeOut = assign.TimeOut,
                            hours = assign.Hours,
                            regular = payrate.Regular,
                            position = assign.Position,
                            title = assign.Title,
                            event_ID = assign.Event_ID,
                            event_name = eventx.Event_name,
                            event_Date = eventx.Event_Date,
                            suffix = staff.Suffix,
                            signIn = assign.SignIn,
                            signOut = assign.SignOut
                        };

            List<DocumentHeaderResponse> couchList = new List<DocumentHeaderResponse>();
            try
            {
                using (var client = new MyCouchClient(pouchDbUrl, "detail_sheet"))
                {
                    foreach (var item in query)
                    {
                        var content = JsonConvert.SerializeObject(item);

                        string id = item.id.ToString();

                        var res = await client.Documents.PutAsync(id, content);

                        couchList.Add(res);
                    }
                }
            }
            catch
            {
                throw;
            }

            return Ok(couchList);
        }

        [Route("api/detailsheets/couch"), HttpGet]
        public async Task<IHttpActionResult> CopyFromCouchToSql(string eventId)
        {
            var assigns = from a in db.Assigns
                          join t in db.Titles on a.Title equals t.Id
                          join p in db.PayRates on t.PayCode equals p.PayCode
                          where a.Event_ID == eventId
                          select new AssignTitlePayRate
                          {
                              Assign = a,
                              Title = t,
                              PayRate = p
                          };

            using (var client = new MyCouchClient(pouchDbUrl, "detail_sheet"))
            {
                var couchQuery = new QueryViewRequest("query", "eventid");
                couchQuery.Configure(x => x
                    .Key(eventId)
                    .IncludeDocs(true)
                    );

                ViewQueryResponse<DetailSheet> response = await client.Views.QueryAsync<DetailSheet>(couchQuery);
                
                foreach (var row in response.Rows)
                {
                    var couchAssignment = JsonConvert.DeserializeObject<DetailSheet>(row.IncludedDoc);
                    var sqlAssignment = assigns.Where(x => x.Assign.Id == couchAssignment.Id).FirstOrDefault();
                    var matchedPayRate = sqlAssignment.PayRate.Regular;

                    if(couchAssignment.Title != sqlAssignment.Assign.Title)
                    {
                        var oldAssignment = new OldAssignment();
                        oldAssignment = DetailSheetService.InsertOldAssignment(oldAssignment, sqlAssignment.Assign);
                        db.OldAssignments.Add(oldAssignment);
                    }

                    sqlAssignment.Assign = DetailSheetService.UpdateCouchToSql(couchAssignment, sqlAssignment.Assign, matchedPayRate);

                    row.Value = couchAssignment;
                    var delete = client.Documents.DeleteAsync(row.Value.Id.ToString(), row.Value._rev);
                }

                await client.Database.CompactAsync();
                await client.Database.ViewCleanupAsync();
            }

            try
            {
                db.SaveChanges();
                return Ok();
            }
            catch
            {
                throw;
            }
        }

        [Route("api/detailsheets/reassignments"), HttpGet]
        public IQueryable<OldAssignment> GetReassignments(string eventId)
        {
            var oldAssignments = db.OldAssignments
                .Where(x => x.EventId == eventId);

            if (oldAssignments.Any())
            {
                return oldAssignments;
            }
            else
            {
                return null;
            }
        }

        [Route("api/detailsheets"), HttpGet]
        public IQueryable<DetailSheet> GetDetailSheets(string eventId)
        {
            var query = from assign in db.Assigns
                        where assign.Event_ID == eventId
                        join title in db.Titles
                            on assign.Title equals title.Id
                        join eventx in db.Events
                            on assign.Event_ID equals eventx.Id
                        join staff in db.Staffs
                            on assign.Staff_ID equals staff.Id
                        join payrate in db.PayRates
                            on title.PayCode equals payrate.PayCode
                        orderby staff.Lname, staff.Fname
                        select new DetailSheet
                        {
                            Id = assign.Id,
                            Gross = assign.Gross,
                            Lname = staff.Lname,
                            Fname = staff.Fname,
                            TimeIn = assign.TimeIn,
                            TimeOut = assign.TimeOut,
                            Hours = assign.Hours,
                            Regular = payrate.Regular,
                            Position = assign.Position,
                            Title = assign.Title,
                            Event_ID = assign.Event_ID,
                            Event_name = eventx.Event_name,
                            Event_Date = eventx.Event_Date,
                            Suffix = staff.Suffix,
                            SignIn = assign.SignIn,
                            SignInTime = assign.SignInTime,
                            SignOut = assign.SignOut,
                            SignOutTime = assign.SignOutTime
                        };

            return query;
        }

    }
}
