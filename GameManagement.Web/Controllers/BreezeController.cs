﻿using Breeze.ContextProvider;
using Breeze.ContextProvider.EF6;
using Breeze.WebApi2;
using GameManagement.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GameManagement.Controllers
{
    [BreezeController]
    public class BreezeController : ApiController
    {
        readonly EFContextProvider<GameMangementEntities> _contextProvider = new EFContextProvider<GameMangementEntities>();

        [HttpGet]
        public string Metadata()
        {
            return _contextProvider.Metadata();
        }

        [HttpGet]
        public IQueryable<Assign> Assign()
        {
            var query = _contextProvider.Context.Assigns;
            return query;
        }

        [HttpGet]
        public IQueryable<Default_title> Default_title()
        {
            var query = _contextProvider.Context.Default_title;
            return query;
        }

        [HttpGet]
        public IQueryable<Event> Event()
        {
            var query = _contextProvider.Context.Events;
            return query;
        }

        [HttpGet]
        public IQueryable<OfficialTitle> OfficialTitle()
        {
            var query = _contextProvider.Context.OfficialTitles;
            return query;
        }

        [HttpGet]
        public IQueryable<PayPeriod> PayPeriod()
        {
            var query = _contextProvider.Context.PayPeriods;

            foreach (var item in query)
            {
                item.StartDate = DateTime.SpecifyKind((DateTime)item.StartDate, DateTimeKind.Local);
                item.EndDate = DateTime.SpecifyKind((DateTime)item.EndDate, DateTimeKind.Local);
            }

            return query;
        }

        [HttpGet]
        public IQueryable<PayRate> PayRate()
        {
            var query = _contextProvider.Context.PayRates;
            return query;
        }

        [HttpGet]
        public IQueryable<Position> Position()
        {
            var query = _contextProvider.Context.Positions;
            return query;
        }

        [HttpGet]
        public IQueryable<Sport> Sport()
        {
            var query = _contextProvider.Context.Sports;
            return query;
        }

        [HttpGet]
        public IQueryable<Staff> Staff()
        {
            var query = _contextProvider.Context.Staffs;
            return query;
        }

        [HttpGet]
        public IQueryable<object> StaffJoinWithTitle([FromUri] string sportId)
        {
            var query = _contextProvider.Context.Staffs
                .Join(_contextProvider.Context.Default_title,
                    a => a.Id,
                    b => b.Staff_ID,
                    (a, b) => new { Staff = a, Title = b })
                .Where(a => a.Staff.Active == true && a.Title.Sport_ID == sportId)
                .Select(a => a.Staff)
                .GroupBy(a => a.Id)
                .Select(a => a.FirstOrDefault());

            return query;
        }

        [HttpGet]
        public IQueryable<object> OfficialJoinWithTitle([FromUri] string sportId)
        {
            var query = _contextProvider.Context.Officials
                .Join(_contextProvider.Context.OfficialTitles,
                    a => a.Id,
                    b => b.Official_ID,
                    (a, b) => new { Staff = a, Title = b })
                .Where(a => a.Staff.Active == true && a.Title.Sport_ID == sportId)
                .Select(a => a.Staff)
                .GroupBy(a => a.Id)
                .Select(a => a.FirstOrDefault());

            return query;
        }

        [HttpGet]
        public IQueryable<Title> Title()
        {
            var query = _contextProvider.Context.Titles;
            return query;
        }

        [HttpGet]
        public IQueryable<TV> Tv()
        {
            var query = _contextProvider.Context.TVs;
            return query;
        }

        [HttpGet]
        public IQueryable<Venue> Venue()
        {
            var query = _contextProvider.Context.Venues;
            return query;
        }




        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            try
            {
                return _contextProvider.SaveChanges(saveBundle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
