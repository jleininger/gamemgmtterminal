﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GameManagement.Models;

namespace GameManagement.Controllers
{
    public class TitlesController : ApiController
    {
        private GameMangementEntities db = new GameMangementEntities();

        // GET: api/Titles
        public IQueryable<Title> GetTitles()
        {
            return db.Titles;
        }

        #region Other methods
        /*
        // GET: api/Titles/5
        [ResponseType(typeof(Title))]
        public async Task<IHttpActionResult> GetTitle(string id)
        {
            Title title = await db.Titles.FindAsync(id);
            if (title == null)
            {
                return NotFound();
            }

            return Ok(title);
        }

        // PUT: api/Titles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTitle(string id, Title title)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != title.Id)
            {
                return BadRequest();
            }

            db.Entry(title).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TitleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Titles
        [ResponseType(typeof(Title))]
        public async Task<IHttpActionResult> PostTitle(Title title)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Titles.Add(title);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TitleExists(title.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = title.Id }, title);
        }

        // DELETE: api/Titles/5
        [ResponseType(typeof(Title))]
        public async Task<IHttpActionResult> DeleteTitle(string id)
        {
            Title title = await db.Titles.FindAsync(id);
            if (title == null)
            {
                return NotFound();
            }

            db.Titles.Remove(title);
            await db.SaveChangesAsync();

            return Ok(title);
        }
        */
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TitleExists(string id)
        {
            return db.Titles.Count(e => e.Id == id) > 0;
        }
    }
}