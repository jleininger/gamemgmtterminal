﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagement.Models.ViewModels
{
    public class EventViewModel
    {
        public string Id { get; set; }
        public string Event_name { get; set; }
        public DateTime? Event_Date { get; set; }
        public DateTime? Event_Time { get; set; }
        public string Event_Sport { get; set; }
        public string Event_Venue { get; set; }
        public string TV { get; set; }
        public DateTime? TimeUtc { get; set; }
        public int? PayPeriodId { get; set; }

        public virtual PayPeriod PayPeriod { get; set; }
    }
}