﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagement.Models
{
    public class DetailSheet
    {
        public int Id { get; set; }
        public decimal? Gross { get; set; }
        public string Lname { get; set; }
        public string Fname { get; set; }
        public DateTime? TimeIn { get; set; }
        public DateTime? TimeOut { get; set; }
        public double? Hours { get; set; }
        public decimal? Regular { get; set; }
        public string Position { get; set; }
        public string Title { get; set; }
        public string Event_ID { get; set; }
        public string Event_name { get; set; }
        public DateTime? Event_Date { get; set; }
        public string Suffix { get; set; }
        public string SignIn { get; set; }
        public DateTime? SignInTime { get; set; }
        public string SignOut { get; set; }
        public DateTime? SignOutTime { get; set; }
        public string _rev { get; set; }
    }
}