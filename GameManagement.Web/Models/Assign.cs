//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GameManagement.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Assign
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Assign()
        {
            this.OldAssignments = new HashSet<OldAssignment>();
        }
    
        public int Id { get; set; }
        public string Staff_ID { get; set; }
        public string Event_ID { get; set; }
        public string Title { get; set; }
        public string Position { get; set; }
        public Nullable<System.DateTime> Report_Time { get; set; }
        public Nullable<System.DateTime> OutTime { get; set; }
        public string PayType { get; set; }
        public Nullable<double> Hours { get; set; }
        public Nullable<decimal> Gross { get; set; }
        public Nullable<decimal> Deduct { get; set; }
        public Nullable<decimal> Net { get; set; }
        public Nullable<System.DateTime> PayPer { get; set; }
        public Nullable<System.DateTime> TimeIn { get; set; }
        public Nullable<System.DateTime> TimeOut { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public string SignIn { get; set; }
        public Nullable<System.DateTime> SignInTime { get; set; }
        public string SignOut { get; set; }
        public Nullable<System.DateTime> SignOutTime { get; set; }
    
        public virtual Title Title1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OldAssignment> OldAssignments { get; set; }
    }
}
