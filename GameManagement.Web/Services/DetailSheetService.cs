﻿using GameManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagement.Services
{
    public class DetailSheetService
    {
        public static Assign UpdateCouchToSql(DetailSheet couchAssignment, Assign sqlAssignment, decimal? payRate)
        {
            sqlAssignment.SignIn = string.IsNullOrEmpty(couchAssignment.SignIn) ? sqlAssignment.SignIn : couchAssignment.SignIn;
            sqlAssignment.SignOut = string.IsNullOrEmpty(couchAssignment.SignOut) ? sqlAssignment.SignOut : couchAssignment.SignOut;

            sqlAssignment.SignInTime = couchAssignment.SignInTime.HasValue ? couchAssignment.SignInTime : sqlAssignment.SignInTime;
            sqlAssignment.SignOutTime = couchAssignment.SignOutTime.HasValue ? couchAssignment.SignOutTime : sqlAssignment.SignOutTime;

            sqlAssignment.Title = couchAssignment.Title;
            sqlAssignment.Position = couchAssignment.Position;
            sqlAssignment.Hours = couchAssignment.Hours;
            sqlAssignment.TimeIn = couchAssignment.TimeIn;
            sqlAssignment.TimeOut = couchAssignment.TimeOut;
            sqlAssignment.Gross = (decimal?)couchAssignment.Hours * payRate;

            return sqlAssignment;
        }

        public static OldAssignment InsertOldAssignment(OldAssignment oldAssignment, Assign assignment)
        {
            oldAssignment.EventId = assignment.Event_ID;
            oldAssignment.AssignId = assignment.Id;
            oldAssignment.TimeIn = assignment.TimeIn;
            oldAssignment.TimeOut = assignment.TimeOut;
            oldAssignment.Position = assignment.Position;
            oldAssignment.TitleId = assignment.Title;
            oldAssignment.Hours = assignment.Hours;
            oldAssignment.Gross = assignment.Gross;

            return oldAssignment;
        }
    }
}