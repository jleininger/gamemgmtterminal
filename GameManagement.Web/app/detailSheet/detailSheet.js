﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'detailSheet';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['common', 'datacontext', '$scope', 'spinner', detailSheet]);

    function detailSheet(common, datacontext, $scope, spinner) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm = this;
        var calculators = common.calculators;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);


        // Bindable properties and functions are placed on vm.
        vm.detailSheet;
        vm.events;
        vm.onSelectEvent = _onSelectEvent;
        vm.onViewClick = _onViewClick;
        vm.selectedEvent;
        vm.title = 'Detail Sheet';


        activate();

        function activate() {
            onDestroy();

            common.activateController([getEvents()], controllerId)
                .then(function () { log('Activated detailSheet View', null, false); })
            ;
        }

        function calculateTotals() {
            vm.eventTotals = calculators.calculateAssignmentTotals(vm.detailSheet);
        }

        function getEvents(forceRefresh) {
            return datacontext.event.getAll(forceRefresh)
                .then(function (data) {
                    vm.events = data;
                    return data;
                });
        }

        function onDestroy() {
            $scope.$on('$destroy', function () {
                datacontext.cancel(false);
            });
        }

        function _onSelectEvent() {
            vm.detailSheet = null;
            vm.reassignments = null;
            vm.oldAssignments = null;
        }

        function _onViewClick() {
            spinner.spinnerShow();

            return datacontext.assign.syncCouchToSql(vm.selectedEvent.id)
                .then(function () {
                    return datacontext.assign.getDetailSheet(vm.selectedEvent.id)
                        .then(function (data) {
                            data.forEach(function (current) {
                                current.timeIn = moment.utc(current.timeIn).format();
                                current.timeOut = moment.utc(current.timeOut).format();
                            });
                            vm.detailSheet = data;
                            calculateTotals();
                        });
                })
                .then(function () {
                    return datacontext.assign.getReassignments(vm.selectedEvent.id)
                        .then(function (data) {
                            if (data) {
                                vm.oldAssignments = data;
                                calculateReassignments();
                            }

                            spinner.spinnerHide();
                        });
                });
        }

        function calculateReassignments() {
            vm.hoursDifference = 0;
            vm.grossDifference = 0;
            vm.reassignments = [];

            vm.oldAssignments.forEach(function (reassignment) {
                var matchedAssignment = vm.detailSheet.find(function (assignment) {
                    return assignment.id === reassignment.assignId;
                });

                vm.reassignments.push(matchedAssignment);
                vm.hoursDifference += (reassignment.hours - matchedAssignment.hours);
                vm.grossDifference += (reassignment.gross - matchedAssignment.gross);
            });
        }


    }
})();
