﻿(function () {
    'use strict';

    var serviceId = 'repository.event';
    angular.module('app').factory(serviceId,
        ['$http', 'model', 'repository.abstract', RepositoryEvent]);

    function RepositoryEvent($http, model, AbstractRepository) {
        var entityName = model.entityNames.event;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.create = create;
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
            this.getById = getById;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        function create() {
            // Include param 'id' because this is a user-generated primary key, so must be included on entity creation
            return this.manager.createEntity(entityName, { id: null });
        }

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
            // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var events = [];

            if (!forceRemote && self._areItemsLoaded()) {
                events = self._getAllLocal(entityName, orderBy);
                return self.$q.when(events);
            }

            return EntityQuery.from('Event')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                events = data.results;
                self.log('Retrieved [Events] from remote data source', events.length, false);
                return events;
            }
        }

        function getById(id, forceRemote) {
            return this._getById(entityName, id, forceRemote);
        }


    }
})();