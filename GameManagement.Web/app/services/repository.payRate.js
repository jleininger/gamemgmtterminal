﻿(function () {
    'use strict';

    var serviceId = 'repository.payrate';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryPayRate]);

    function RepositoryPayRate(model, AbstractRepository) {
        var entityName = model.entityNames.payRate;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, payCode) {
            var self = this;
            var payRates = [];

            var predicate = Predicate
                .create('payCode', '==', payCode);

            if (!forceRemote && self._areItemsLoaded()) {
                payRates = self._getAllLocal(entityName, orderBy, predicate);
                return self.$q.when(payRates);
            }

            return EntityQuery.from('PayRate')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                payRates = data.results;
                self.log('Retrieved [Pay Rates] from remote data source', payRates.length, false);
                return payRates;
            }
        }


    }
})();