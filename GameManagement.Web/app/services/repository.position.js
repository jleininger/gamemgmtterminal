﻿(function () {
    'use strict';

    var serviceId = 'repository.position';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryPosition]);

    function RepositoryPosition(model, AbstractRepository) {
        var entityName = model.entityNames.position;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'position1';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var positions = [];

            if (!forceRemote && self._areItemsLoaded()) {
                positions = self._getAllLocal(entityName, orderBy);
                return self.$q.when(positions);
            }

            return EntityQuery.from('Position')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                positions = data.results;
                self.log('Retrieved [positions] from remote data source', positions.length, false);
                return positions;
            }
        }


    }
})();