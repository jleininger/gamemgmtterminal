﻿(function () {
    'use strict';

    var serviceId = 'repository.staff';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryStaff]);

    function RepositoryStaff(model, AbstractRepository) {
        var entityName = model.entityNames.staff;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'lname';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
            this.getJoinedWithTitle = getJoinedWithTitle;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
            // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var staffs = [];

            if (!forceRemote && self._areItemsLoaded()) {
                staffs = self._getAllLocal(entityName, orderBy);
                return self.$q.when(staffs);
            }

            return EntityQuery.from('Staff')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                staffs = data.results;
                self.log('Retrieved [staffs] from remote data source', staffs.length, false);
                return staffs;
            }
        }

        function getJoinedWithTitle(forceRemote, sportId) {
            var self = this;
            var staffs = [];

            if (!forceRemote && self._areItemsLoaded()) {
                staffs = self._getAllLocal(entityName, orderBy);
                return self.$q.when(staffs);
            }

            return EntityQuery.from('StaffJoinWithTitle')
                .orderBy(orderBy)
                .toType(entityName)
                .withParameters({sportId: sportId})
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                staffs = data.results;
                self.log('Retrieved [staffs] from remote data source', staffs.length, false);
                return staffs;
            }
        }


    }
})();