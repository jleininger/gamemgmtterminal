﻿(function () {
    'use strict';

    var serviceId = 'repository.payperiod';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryPayRate]);

    function RepositoryPayRate(model, AbstractRepository) {
        var entityName = model.entityNames.payPeriod;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id desc';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, payCode) {
            var self = this;
            var payPeriods = [];

            //var predicate = Predicate
            //    .create('pay_Code', '==', payCode);

            if (!forceRemote && self._areItemsLoaded()) {
                payPeriods = self._getAllLocal(entityName, orderBy);
                return self.$q.when(payPeriods);
            }

            return EntityQuery.from('PayPeriod')
                .orderBy(orderBy)
                .take(60)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                payPeriods = data.results;
                self.log('Retrieved [Pay Periods] from remote data source', payPeriods.length, false);
                return payPeriods;
            }
        }


    }
})();