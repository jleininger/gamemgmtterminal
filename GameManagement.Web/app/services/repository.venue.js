﻿(function () {
    'use strict';

    var serviceId = 'repository.venue';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryVenue]);

    function RepositoryVenue(model, AbstractRepository) {
        var entityName = model.entityNames.venue;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var venues = [];

            if (!forceRemote && self._areItemsLoaded()) {
                venues = self._getAllLocal(entityName, orderBy);
                return self.$q.when(venues);
            }

            return EntityQuery.from('venue')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                venues = data.results;
                self.log('Retrieved [venues] from remote data source', venues.length, false);
                return venues;
            }
        }


    }
})();