﻿(function () {
    'use strict';

    var serviceId = 'repository.tv';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryTv]);

    function RepositoryTv(model, AbstractRepository) {
        var entityName = model.entityNames.tv;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var tvs = [];

            if (!forceRemote && self._areItemsLoaded()) {
                tvs = self._getAllLocal(entityName, orderBy);
                return self.$q.when(tvs);
            }

            return EntityQuery.from('Tv')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                tvs = data.results;
                self.log('Retrieved [tvs] from remote data source', tvs.length, false);
                return tvs;
            }
        }


    }
})();