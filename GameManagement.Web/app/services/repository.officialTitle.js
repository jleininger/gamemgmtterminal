﻿(function () {
    'use strict';

    var serviceId = 'repository.officialtitle';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryOfficialTitle]);

    function RepositoryOfficialTitle(model, AbstractRepository) {
        var entityName = model.entityNames.officialTitle;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'preference, position';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, officialId, sportId) {
            var self = this;
            var officialTitles = [];

            var predicate = Predicate
                .create('official_ID', '==', officialId)
                .and('sport_ID', '==', sportId);

            if (!forceRemote && self._areItemsLoaded()) {
                officialTitles = self._getAllLocal(entityName, orderBy, predicate);
                return self.$q.when(officialTitles);
            }

            return EntityQuery.from('OfficialTitle')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                officialTitles = data.results;
                self.log('Retrieved [Official Titles] from remote data source', officialTitles.length, false);
                return officialTitles;
            }
        }


    }
})();