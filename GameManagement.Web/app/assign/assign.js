﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'assign';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['bootstrap.dialog', 'common', 'config', 'datacontext', '$q', '$scope', assign]);

    function assign(bsDialog, common, config, datacontext, $q, $scope) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm = this;
        var calculators = common.calculators;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var matchedPayRate;
        var matchedTitle;

        // Bindable properties and functions are placed on vm.
        vm.assignedStaff;
        vm.assignmentsByStaffId;
        vm.eventTotals = {};
        vm.events;
        vm.defaultTitles;
        vm.onSave = _onSave;
        vm.onSelectDefaultTitle = _onSelectDefaultTitle;
        vm.onSelectEvent = _onSelectEvent;
        vm.onSelectStaff = _onSelectStaff;
        vm.onSelectStaffOrOfficial = _onSelectStaffOrOfficial;
        vm.positions;
        vm.removeStaff = _removeStaff;
        vm.selectedDefaultTitle;
        vm.selectedEvent;
        vm.selectedPosition;
        vm.selectedStaff;
        vm.staff;
        vm.staffOrOfficial;
        vm.title = 'Assign';


        activate();

        function activate() {
            onDestroy();

            common.activateController([getEvents(), getDefaultTitles(), getOfficialTitles(), getPayRates(), getPositions(), getTitles()], controllerId)
                .then(function () { log('Activated Assign View', null, false); })
            ;
        }

        function calculateAssignmentTotals() {
            vm.eventTotals = calculators.calculateAssignmentTotals(vm.assignedStaff);
        }

        function getAssignmentsByStaffId() {
            return datacontext.assign.getByStaffId(vm.selectedStaff.id, 10)
                .then(function (data) {
                    vm.assignmentsByStaffId = data;
                });
        }

        function getCurrentAssigned(forceRefresh) {
            return datacontext.assign.getAll(forceRefresh, vm.selectedEvent.id)
                .then(function (data) {
                    vm.assignedStaff = data;
                    calculateAssignmentTotals();
                });
        }

        function getDefaultTitles(forceRefresh, staffId, sportId) {
            return datacontext.defaultTitle.getAll(forceRefresh, staffId, sportId)
                .then(function (data) {
                    vm.defaultTitles = data;
                    initSelectedDefaultTitle();
                    initSelectedPosition();
                    return data;
                });
        }

        function getOfficialTitles(forceRefresh, staffId, sportId) {
            return datacontext.officialTitle.getAll(forceRefresh, staffId, sportId)
                .then(function (data) {
                    vm.defaultTitles = data;
                    initSelectedDefaultTitle();
                    initSelectedPosition();
                    return data;
                });
        }

        function getEvents(forceRefresh) {
            return datacontext.event.getAll(forceRefresh)
                .then(function (data) {
                    vm.events = data;
                    return data;
                });
        }

        function getPayRates(forceRefresh, payCode) {
            return datacontext.payRate.getAll(forceRefresh, payCode)
                .then(function (data) {
                    matchedPayRate = data;
                    return data;
                });
        }

        function getPositions(forceRefresh) {
            return datacontext.position.getAll(forceRefresh)
                .then(function (data) {
                    vm.positions = data;
                    return data;
                });
        }

        function getStaffJoinedWithTitle(forceRefresh, sportId) {
            return datacontext.staff.getJoinedWithTitle(forceRefresh, sportId)
                .then(function (data) {
                    vm.staff = data;
                    return data;
                });
        }

        function getOfficialJoinedWithTitle(forceRefresh, sportId) {
            return datacontext.official.getJoinedWithTitle(forceRefresh, sportId)
                .then(function (data) {
                    vm.officials = data;
                    return data;
                });
        }

        function getTitles(forceRefresh, id) {
            return datacontext.title.getAll(forceRefresh, id)
                .then(function (data) {
                    matchedTitle = data;
                    return data;
                });
        }

        function initSelectedDefaultTitle() {
            if (vm.selectedEvent) {
                vm.selectedDefaultTitle = vm.defaultTitles[0];
            }
        }


        function initSelectedPosition() {
            if (vm.selectedDefaultTitle) {
                for (var i = 0; i < vm.positions.length; i++) {
                    if (vm.positions[i].position1 === vm.selectedDefaultTitle.position) {
                        vm.selectedPosition = vm.positions[i];
                        break;
                    }
                }
                if (!vm.selectedPosition) {
                    vm.selectedPosition = {};
                }
            }
        }

        function onDestroy() {
            $scope.$on('$destroy', function () {
                datacontext.cancel(false);
            });
        }

        function _onSave() {
            var personToAssign = datacontext.assign.create();

            buildAssignObject();



            function buildAssignObject() {

                getTitles(false, vm.selectedDefaultTitle.title_ID, vm.selectedPosition.position1)
                   .then(function () {
                        getPayRates(false, matchedTitle[0].payCode);
                    })
                  .then(function () {
                      personToAssign.staff_ID = vm.selectedStaff.id;
                      personToAssign.event_ID = vm.selectedEvent.id;
                      personToAssign.title = vm.selectedDefaultTitle.title_ID;
                      personToAssign.position = vm.selectedPosition.position1;
                      personToAssign.payPer = null;

                      if (vm.staffOrOfficial === 'staff') {
                          personToAssign.gross = matchedPayRate[0].regCalc * matchedTitle[0].rhours; // null
                          personToAssign.timeIn = vm.selectedEvent.timeUtc ? moment(vm.selectedEvent.timeUtc).add(matchedTitle[0].report * -60, 'minutes').format() : null; // null
                          personToAssign.timeOut = vm.selectedEvent.timeUtc ? moment(personToAssign.timeIn).add(matchedTitle[0].rhours * 60, 'minutes').format() : null; // null
                          personToAssign.payType = vm.selectedStaff.pay_Cycle; // "OFFICIALS"
                          personToAssign.hours = matchedTitle[0].rhours; // 0
                      }
                      else if (vm.staffOrOfficial === 'official') {
                          personToAssign.payType = "OFFICIALS";
                          personToAssign.hours = 0;
                      }


                      return $q.when(personToAssign);
                  })
                .then(save);
            }
        }

        function _onSelectDefaultTitle() {
            //vm.selectedPosition = null;
        }

        function _onSelectEvent() {
            vm.assignedStaff = null;
            vm.selectedStaff = null;
            vm.selectedDefaultTitle = null;
            vm.selectedPosition = null;
            vm.assignmentsByStaffId = null;
            vm.eventTotals.positions = null;
            vm.staffOrOfficial = null;

            getCurrentAssigned(true);
        }

        function _onSelectStaff() {
            vm.selectedDefaultTitle = null;
            vm.selectedPosition = null;
            vm.assignmentsByStaffId = null;

            if (vm.staffOrOfficial === 'staff') {
                getDefaultTitles(false, vm.selectedStaff.id, vm.selectedEvent.event_Sport);
            }
            else if (vm.staffOrOfficial === 'official') {
                getOfficialTitles(false, vm.selectedStaff.id, vm.selectedEvent.event_Sport);
            }

            getAssignmentsByStaffId();
        }

        function _onSelectStaffOrOfficial() {
            vm.selectedStaff = null;
            vm.selectedDefaultTitle = null;
            vm.selectedPosition = null;
            vm.assignmentsByStaffId = null;

            if (vm.staffOrOfficial === 'staff') {
                getStaffJoinedWithTitle(true, vm.selectedEvent.event_Sport);
            }
            else if (vm.staffOrOfficial === 'official') {
                getOfficialJoinedWithTitle(true, vm.selectedEvent.event_Sport);
            }

            
        }

        function refresh() { getEvents(true); }

        function _removeStaff(assign) {
            bsDialog.confirmationDialog('Remove Assignment?', 'Remove ' + assign.staff_ID + ' from ' + vm.selectedEvent.event_name + '?')
                .then(confirmRemove);

            function confirmRemove() {
                datacontext.markDeleted(assign);
                save().then(null, failed);

                function failed() {
                    datacontext.cancel();
                }
            }
        }

        function save() {
            return datacontext.save()
                .then(function (saveResult) {
                    getCurrentAssigned(true);
                }, function (error) {
                    datacontext.cancel();
                });
        }



    }
})();
