﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('DetailSheet', DetailSheet);

    DetailSheet.$inject = ['Assign', 'Pouch', '$state'];

    function DetailSheet(Assign, Pouch, $state) {
        var vm = this;
        var selectedEvent;

        vm.detailSheet;
        vm.onClearSearch = _onClearSearch;
        vm.onEmployeeClick = _onEmployeeClick;
        vm.search = {};


        init();


        function init() {
            query();
        }

        function query() {
            Pouch.getSelectedEvent()
                .then(function (data) {
                    selectedEvent = data;

                    Pouch.queryByEventId(selectedEvent.value)
                            .then(function (data) { 
                                vm.detailSheet = data;
                            });
                });
        }

        function _onClearSearch() {
            vm.search = {};
        }

        function _onEmployeeClick(item) {
            Assign.setSelectedEmployee(item);

            $state.go('signature', null);
        }

    }

})();
