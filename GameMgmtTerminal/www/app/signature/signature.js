﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Signature', Signature);

    Signature.$inject = ['Assign', 'Pouch', '$state', '$http'];

    function Signature(Assign, Pouch, $state, $http) {
        var signInCanvas;
        var signOutCanvas;
        var vm = this;

        vm.clockInOrOut;
        vm.employee;
        vm.selectedEmployee = Assign.selectedEmployee;

        init();

        function init() {
            initsignInPad();
            vm.selectedEmployee.timeInFormatted = moment.utc(vm.selectedEmployee.doc.timeIn).format();
            vm.selectedEmployee.timeOutFormatted = moment.utc(vm.selectedEmployee.doc.timeOut).format();
            console.log('selected emp formz', vm.selectedEmployee);
        }

        function saveSignature() {
            Pouch.updateDetailSheet(vm.selectedEmployee.doc)
                .then(function (result) {
                    $state.go('detailSheet', null);
                });

        }


        function initCanvasSize() {
            // var context = canvas.getContext('2d'); // Is this line needed? context isnt used

            // resize the canvas to fill browser window dynamically
            window.addEventListener('resize', resizeCanvas, false);

            resizeCanvas();

            function resizeCanvas() {
                signInCanvas.width = window.innerWidth * .9;
                signOutCanvas.width = window.innerWidth * .9;
            }
        }

        function initsignInPad() {
            signInCanvas = document.getElementById('signInCanvas');
            signOutCanvas = document.getElementById('signOutCanvas');

            var signInPad = new SignaturePad(signInCanvas);
            var signOutPad = new SignaturePad(signOutCanvas);

            vm.clearSignIn = function () {
                signInPad.clear();
            }

            vm.saveSignIn = function () {
                vm.selectedEmployee.doc.signInTime = moment().format();
                vm.selectedEmployee.doc.signIn = signInPad.toDataURL();
                saveSignature();
            }

            vm.clearSignOut = function () {
                signOutPad.clear();
            }

            vm.saveSignOut = function () {
                vm.selectedEmployee.doc.signOutTime = moment().format();
                vm.selectedEmployee.doc.signOut = signOutPad.toDataURL();
                saveSignature();
            }

            initCanvasSize();
        }

    }

})();
