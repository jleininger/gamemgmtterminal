﻿(function () {
    'use strict';

    angular.module('app')

    .service('ErrorLogSvc', ErrorLogSvc);

    ErrorLogSvc.$inject = ['$localstorage'];

    function ErrorLogSvc($localstorage) {
        var service = {
            clear: _clear,
            log: _log
        };

        return service;

        function _clear() {
            var errorWrapper = {
                data: []
            };

            $localstorage.setObject('GameMgmtErrors', errorWrapper);
        }

        function _log(errorData) {
            var errorWrapper = {
                data: []
            };

            var errorLog = {
                date: Date(),
                cause: errorData.cause || null,
                stack: new Error(),
                fullError: errorData
            };

            if (errorData.data) {
                errorLog.message = errorData.data.message || null;
                errorLog.exceptionMessage = errorData.data.exceptionMessage || null;

                if (errorData.data.innerException) {
                    errorLog.innerExceptionMsg = errorData.data.innerException.exceptionMessage || null;
                }
            }


            var GameMgmtErrors = $localstorage.getObject('GameMgmtErrors') || {
                data: []
            };
            if (GameMgmtErrors.data && GameMgmtErrors.data.length > 0) {
                GameMgmtErrors.data.unshift(errorLog);
                errorWrapper.data = GameMgmtErrors.data;
            }
            else {
                errorWrapper.data = [errorLog];
            }

            $localstorage.setObject('GameMgmtErrors', errorWrapper);
        }
    }


})();
