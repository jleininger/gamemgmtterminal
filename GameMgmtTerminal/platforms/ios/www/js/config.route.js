﻿(function () {
    'use strict';

    angular.module('app')
        .config(RouteConfig);

    RouteConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', '$httpProvider'];

    function RouteConfig($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
        $ionicConfigProvider.views.maxCache(0);

        $stateProvider
            .state('detailSheet', {
                url: '/detailSheet',
                templateUrl: 'app/detailSheet/detailSheet.html',
                //abstract: true,
                controller: 'DetailSheet as vm'
            })
            .state('errorReport', {
                url: '/errorReport',
                templateUrl: 'app/errorReport/errorReport.html',
                controller: 'ErrorReport as vm'
            })
            .state('manager', {
                url: '/manager',
                templateUrl: 'app/manager/manager.html',
                controller: 'Manager as vm'
            })
            .state('signature', {
                url: '/signature',
                templateUrl: 'app/signature/signature.html',
                controller: 'Signature as vm'
            })
            
        ;

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/detailSheet');

    }
     


})();