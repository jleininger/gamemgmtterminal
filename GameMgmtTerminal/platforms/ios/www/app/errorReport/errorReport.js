﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ErrorReport', ErrorReport);

    ErrorReport.$inject = ['ErrorLogSvc', '$ionicPopup', '$localstorage'];

    function ErrorReport(ErrorLogSvc, $ionicPopup, $localstorage) {
        var vm = this;

        vm.errors = {};

        vm.deleteLog = _deleteLog;
        vm.parseDate = _parseDate;
        vm.refresh = init;

        init();

        function init() {
            var errors = $localstorage.getObject('GameMgmtErrors');
            vm.errors = errors.data;
        }

        function _deleteLog() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Confirm Delete',
                template: 'Are you sure you want to delete this error log?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    ErrorLogSvc.clear();
                    init();
                } 
            });

            
        }

        function _parseDate(dateStr) {
            return moment(dateStr).format('MM-DD-YY, h:mm a');
        }
    }

})();
