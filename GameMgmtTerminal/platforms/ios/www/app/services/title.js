﻿(function () {
    'use strict';

    angular.module('app')

    .factory('Title', Title);

    Title.$inject = ['BaseUrls', '$http', 'IonicAlertSvc', '$q'];

    function Title(BaseUrls, $http, IonicAlertSvc, $q) {
        var service = {
            getAll: getAll
        };

        return service;


        function getAll() {
            return $http.get(BaseUrls.webServer + 'Titles/')
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    var msg = 'Error getting list of titles.';
                    if (error.data && error.data.message) {
                        msg = msg + '\n' + error.data.message;
                        error.data.message = msg;
                    }
                    IonicAlertSvc.error(msg, error);
                    return $q.reject(error)
                });
        }

    }

})();
