﻿(function () {
    'use strict';

    angular.module('app')

    .factory('DefaultTitle', DefaultTitle);

    DefaultTitle.$inject = ['BaseUrls', '$http', 'IonicAlertSvc', '$q'];

    function DefaultTitle(BaseUrls, $http, IonicAlertSvc, $q) {
        var service = {
            getAll: getAll
        };

        return service;


        function getAll() {
            return $http.get(BaseUrls.webServer + 'default_titles')
                .then(function (result) {
                    result.data.forEach(function (item) {
                        item._id = item.id.toString();
                    });

                    return result.data;
                })
                .catch(function (error) {
                    var msg = 'Error getting Default Titles.';
                    if (error.data && error.data.message) {
                        msg = msg + '\n' + error.data.message;
                        error.data.message = msg;
                    }
                    IonicAlertSvc.error(msg, error);
                    return $q.reject(error)
                });
        }

    }

})();
