﻿(function () {
    'use strict';

    angular.module('app')

    .factory('Position', Position);

    Position.$inject = ['BaseUrls', '$http', 'IonicAlertSvc', '$q'];

    function Position(BaseUrls, $http, IonicAlertSvc, $q) {
        var service = {
            getAll: getAll
        };

        return service;


        function getAll() {
            return $http.get(BaseUrls.webServer + 'positions')
                .then(function (result) {
                    result.data.forEach(function (item) {
                        item._id = item.id.toString();
                    });

                    return result.data;
                })
                .catch(function (error) {
                    var msg = 'Error getting positions.';
                    if (error.data && error.data.message) {
                        msg = msg + '\n' + error.data.message;
                        error.data.message = msg;
                    }
                    IonicAlertSvc.error(msg, error);
                    return $q.reject(error)
                });
        }

    }

})();
