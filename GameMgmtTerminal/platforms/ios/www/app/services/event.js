﻿(function () {
    'use strict';

    angular.module('app')

    .factory('Event', Event);

    Event.$inject = ['BaseUrls', '$http', 'IonicAlertSvc', '$q'];

    function Event(BaseUrls, $http, IonicAlertSvc, $q) {
        var service = {
            getAll: getAll
        };

        return service;


        function getAll() {
            return $http.get(BaseUrls.webServer + 'Events/')
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    var msg = 'Error getting list of events.';
                    if (error.data && error.data.message) {
                        msg = msg + '\n' + error.data.message;
                        error.data.message = msg;
                    }
                    IonicAlertSvc.error(msg, error);
                    return $q.reject(error)
                });
        }

    }

})();
