﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Manager', Manager);

    Manager.$inject = ['Assign', 'DefaultTitle', 'Event', 'IonicAlertSvc', '$ionicModal', '$ionicScrollDelegate', 'Position', 'Pouch', '$q', '$scope', 'Spinner', '$state', 'Title'];

    function Manager(Assign, DefaultTitle, Event, IonicAlertSvc, $ionicModal, $ionicScrollDelegate, Position, Pouch, $q, $scope, Spinner, $state, Title) {
        var vm = this;

        var _selectedEventObj;

        vm.alreadyGenerated = false;
        vm.closeModal = _closeModal;
        vm.defaultTitles;
        vm.detailSheet;
        vm.detailSheetGenerated = Assign.detailSheetGenerated;
        vm.events;
        vm.newPosition;
        vm.newTitle;
        vm.onGenerateDetailSheet = _onGenerateDetailSheet;
        vm.onReassignClose = _onReassignClose;
        vm.onLoadStaff = _onLoadStaff;
        vm.onSelectEvent = _onSelectEvent;
        vm.onStaffClick = _onStaffClick;
        vm.onSyncClick = _onSyncClick;
        vm.onSyncDefaultTitles = _onSyncDefaultTitles;
        vm.onSyncEvents = _onSyncEvents;
        vm.onSyncPositions = _onSyncPositions;
        vm.onSyncTitles = _onSyncTitles;
        vm.positions;
        vm.selectedEvent;
        vm.selectedStaff;
        vm.showReassignByPosition = false;
        vm.showReassignAlphabetical = false;
        vm.updateStaff = _updateStaff;

        init();


        function init() {
            $q.all([
                getAllEvents(),
                getAllTitles(),
                getAllPositions()
            ]).then(initSelectedEvent);

            loadModal();
        }

        function checkIfAlreadyGenerated() {
            Pouch.getAllDetailSheet()
                .then(function (data) {
                    vm.alreadyGenerated = data.some(function (val) {
                        return val.doc.event_ID === vm.selectedEvent;
                    });
                });
        }

        function _closeModal() {
            $scope.modal.hide();
        }

        function deleteAll() {
            return Pouch.deleteAll();
        }

        function getAllEvents() {
            return Pouch.getAllEvents()
                .then(function (data) {
                    vm.events = data;
                });
        }

        function getAllPositions() {
            return Pouch.getAllPositions()
                .then(function (data) {
                    vm.positions = data;
                });
        }

        function getAllTitles() {
            return Pouch.getAllTitles()
                .then(function (data) {
                    vm.titles = data;
                });
        }

        function initSelectedEvent() {
            Pouch.getSelectedEvent()
                .then(function (data) {
                    if (data.value) {
                        vm.selectedEvent = data.value;
                        _selectedEventObj = matchSelectedEvent();
                    }
                })
                .then(checkIfAlreadyGenerated);
        }

        function loadModal() {
            $ionicModal.fromTemplateUrl('app/manager/reassign.html', {
                scope: $scope,
                animation: 'slide-in-up',
                focusFirstInput: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $scope.$on('$destroy', function () {
                $scope.modal.remove();
            });
        }

        function matchSelectedEvent() {
            if (vm.selectedEvent) {
                for (var i = 0; i < vm.events.length; i++) {
                    if (vm.events[i].doc.id === vm.selectedEvent) {
                        return vm.events[i].doc;
                    }
                }
            }
        }

        function _onGenerateDetailSheet() {
            Spinner.show();

            return Assign.getDetailSheet(vm.selectedEvent)
                .then(Pouch.syncDetailSheet)
                .then(function () {
                    vm.alreadyGenerated = true;
                    Spinner.hide();
                });
        }

        function _onLoadStaff(sorting) {
            Spinner.show();

            Pouch.queryByEventId(vm.selectedEvent)
                .then(function (data) {
                    if (sorting === 'alpha') {
                        vm.showReassignByPosition = false;
                        vm.showReassignAlphabetical = true;
                        vm.detailSheet = data;
                        Spinner.hide();
                    }
                    else if (sorting === 'position') {
                        var groups = {};

                        for (var i = 0; i < data.length; i++) {
                            var position = data[i].doc.position;
                            if (!groups[position]) {
                                groups[position] = [];
                            }
                            groups[position].push(data[i]);
                        }

                        var detailSheet = [];
                        for (var position in groups) {
                            detailSheet.push({ position: position, values: groups[position] });
                        }

                        vm.showReassignAlphabetical = false;
                        vm.showReassignByPosition = true;
                        vm.detailSheet = detailSheet;
                        Spinner.hide();
                    }

                });
        }

        function _onReassignClose(shouldScroll) {
            vm.detailSheet = null;
            vm.showReassignByPosition = false;
            vm.showReassignAlphabetical = false;

            if (shouldScroll) {
                $ionicScrollDelegate.scrollTop();
            }
        }

        function _onSelectEvent() {
            _selectedEventObj = matchSelectedEvent();

            Pouch.getSelectedEvent()
                .then(function (data) {
                    var selectedEvent = data;
                    selectedEvent.value = vm.selectedEvent.toString();
                    Pouch.updateEvent(selectedEvent);

                    checkIfAlreadyGenerated();
                });


        }

        function _onStaffClick(staff) {
            Spinner.show();
            vm.selectedStaff = staff;
            vm.newPosition = vm.selectedStaff.doc.position;
            vm.newTitle = vm.selectedStaff.doc.title;

            Pouch.queryDefaultTitlesbyStaffId(staff.doc.staff_ID)
                .then(function (data) {
                    vm.defaultTitles = data;
                    Spinner.hide();
                    openModal();
                });
        }

        function _onSyncClick() {
            Spinner.show();

            return Pouch.syncDetailSheet()
                .then(Pouch.deleteDetailSheetDb)
                .then(function () {
                    vm.alreadyGenerated = false;
                    Spinner.hide();
                })
                .catch(function (err) {
                    var msg = err && err.message ? err.message : err;
                    Spinner.hide();
                    IonicAlertSvc.error('Sync error.', msg);
                })
            ;
        }

        function _onSyncDefaultTitles() {
            Spinner.show();

            return DefaultTitle.getAll()
                .then(Pouch.bulkDefaultTitles)
                .then(function () {
                    Spinner.hide();
                });
        }

        function _onSyncEvents() {
            Spinner.show();

            return Event.getAll()
                .then(Pouch.bulkEvents)
                .then(getAllEvents)
                .then(function () {
                    Spinner.hide();
                });
        }

        function _onSyncPositions() {
            Spinner.show();

            return Position.getAll()
                .then(Pouch.bulkPositions)
                .then(getAllPositions)
                .then(function () {
                    Spinner.hide();
                });
        }

        function _onSyncTitles() {
            Spinner.show();

            return Title.getAll()
                .then(Pouch.bulkTitles)
                .then(getAllTitles)
                .then(function () {
                    Spinner.hide();
                });
        }

        function openModal() {
            $scope.modal.show();
        }

        function _updateStaff() {
            vm.selectedStaff.doc.position = vm.newPosition;
            vm.selectedStaff.doc.title = vm.newTitle;

            var _matchedTitle = matchTitle();

            vm.selectedStaff.doc.timeIn = _selectedEventObj.timeUtc ? moment(_selectedEventObj.timeUtc).add(_matchedTitle.report * -60, 'minutes').format('YYYY-MM-DDTHH:mm:ss') : null;

            vm.selectedStaff.doc.timeOut = _selectedEventObj.timeUtc ? moment(vm.selectedStaff.doc.timeIn).add(_matchedTitle.rhours * 60, 'minutes').format('YYYY-MM-DDTHH:mm:ss') : null;

            vm.selectedStaff.doc.hours = _matchedTitle.rhours;

            console.log(vm.selectedStaff);

            Pouch.updateDetailSheet(vm.selectedStaff.doc)
                .then(_closeModal);

            function matchTitle() {
                for (var i = 0; i < vm.titles.length; i++) {
                    if (vm.titles[i].doc.id === vm.selectedStaff.doc.title) {
                        return _matchedTitle = vm.titles[i].doc;
                    }
                }
            }
        }

    }

})();
//function{ } { }