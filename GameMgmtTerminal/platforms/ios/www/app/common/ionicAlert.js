﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('IonicAlertSvc', IonicAlertSvc);

    IonicAlertSvc.$inject = ['$ionicPopup', 'ErrorLogSvc', 'Spinner'];

    function IonicAlertSvc($ionicPopup, ErrorLogSvc, Spinner) {
        var service = {
            alert: _alert,
            confirm: _confirm,
            error: _error
        };

        return service;

        function _alert(msg) {
            Spinner.hide();

            var opts = {};
            var defaultMsg = "Error";

            if (msg) {
                opts.title = msg.title ? msg.title : defaultMsg;
                opts.template = msg.template ? msg.template : '';
            } else { // Default error
                opts.title = defaultMsg;
            }

            var iAlert = $ionicPopup.alert(opts);
        }

        function _confirm(msg, onConfirmFn) {
            var opts = {};

            if (msg && msg.title) {
                opts.title = msg.title;
            } else {
                opts.title = "Confirm";
            }

            if (msg && msg.template) {
                opts.template = msg.template;
            }

            var confirmPopup = $ionicPopup.confirm(opts);

            confirmPopup.then(function (res) {
                Spinner.hide();

                if (res) {
                    onConfirmFn();
                }
            });
        }


        // Log error then call Ionic alert
        function _error(errMsg, error) {

            var msg = {
                title: 'Error',
                template: errMsg || ''
            };

            ErrorLogSvc.log(error);

            return service.alert(msg);
        }
    }
})();
