﻿(function () {
    'use strict';

    angular.module('app')
    .factory('Spinner', Spinner);

    Spinner.$inject = ['$ionicLoading'];

    function Spinner ($ionicLoading) {

        var service = {
            show: _show,
            hide: _hide
        }

        return service;

        function _show() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>',
                delay: 400,
                duration: 20000
            });
        }

        function _hide() {
            $ionicLoading.hide();
        }
    }


})();
