﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameMgmtTerminal.Web.Startup))]
namespace GameMgmtTerminal.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
