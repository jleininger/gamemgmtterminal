﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GameMgmtTerminal.Web.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;
using MyCouch.Requests;
using MyCouch.Responses;
using MyCouch;

namespace GameMgmtTerminal.Web.Controllers
{
    public class AssignsController : ApiController
    {
        private GameMangement_EDAEntities db = new GameMangement_EDAEntities();
        private string pouchDbUrl = "http://localhost:5984";

        [Route("api/assigns/detailsheet"), HttpGet]
        public async Task<IHttpActionResult> GetDetailSheet(string eventId)
        {
            var query = from assign in db.Assigns
                        where assign.Event_ID == eventId
                        join title in db.Titles
                            on assign.Title equals title.Id
                        join eventx in db.Events
                            on assign.Event_ID equals eventx.Id
                        join staff in db.Staffs
                            on assign.Staff_ID equals staff.Id
                        join payrate in db.PayRates
                            on title.Pay_Code equals payrate.Pay_Code
                        orderby staff.Lname, staff.Fname
                        select new
                        {
                            assign.Id,
                            assign.Gross,
                            staff.Lname,
                            staff.Fname,
                            assign.TimeIn,
                            assign.TimeOut,
                            assign.Hours,
                            payrate.Regular,
                            assign.Position,
                            assign.Event_ID,
                            eventx.Event_name,
                            eventx.Event_Date,
                            staff.Suffix,
                            assign.Signature
                        };

            List<DocumentHeaderResponse> couchList = new List<DocumentHeaderResponse>();
            try
            {
                using (var client = new MyCouchClient(pouchDbUrl, "detail_sheet"))
                {
                    foreach (var item in query)
                    {
                        var content = JsonConvert.SerializeObject(item);

                        //await client.Documents.PostAsync(content);

                        string id = item.Id.ToString();

                        var res = await client.Documents.PutAsync(id, content);

                        couchList.Add(res);
                    }
                }

            }
            catch
            {
                throw;
            }

            return Ok(couchList);
        }

        [Route("api/assigns/saveSignatures"), HttpGet]
        public async Task<IHttpActionResult> SaveSignatures()
        {
            using (var client = new MyCouchClient(pouchDbUrl, "detail_sheet"))
            {
                var queryAll = new QueryViewRequest("_all_docs");
                queryAll.IncludeDocs = true;
                ViewQueryResponse<string> response = await client.Views.QueryAsync<string>(queryAll);

                foreach (var row in response.Rows)
                {
                    var newAssign = JsonConvert.DeserializeObject<AbbreviatedAssign>(row.IncludedDoc);

                    var currentAssign = db.Assigns.Where(x => x.Id == newAssign.Id).FirstOrDefault();

                    currentAssign.Signature = newAssign.Signature;
                }

                try
                {
                    db.SaveChanges();
                }
                catch
                {
                    throw;
                }
                return StatusCode(HttpStatusCode.NoContent);
            }
        }

        private class AbbreviatedAssign
        {
            public int Id { get; set; }
            public string Signature { get; set; }
        }


        //// GET: api/Assigns
        //public IQueryable<Assign> GetAssigns()
        //{
        //    return db.Assigns;
        //}


        // GET: api/Assigns/5
        [ResponseType(typeof(Assign))]
        public IHttpActionResult GetAssign(int id)
        {
            Assign assign = db.Assigns.Find(id);
            if (assign == null)
            {
                return NotFound();
            }

            return Ok(assign);
        }

        // PUT: api/Assigns/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssign(int id, Assign assign)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assign.Id)
            {
                return BadRequest();
            }

            db.Entry(assign).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssignExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/Assigns
        //[ResponseType(typeof(Assign))]
        //public IHttpActionResult PostAssign(Assign assign)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Assigns.Add(assign);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = assign.Id }, assign);
        //}

        //// DELETE: api/Assigns/5
        //[ResponseType(typeof(Assign))]
        //public IHttpActionResult DeleteAssign(int id)
        //{
        //    Assign assign = db.Assigns.Find(id);
        //    if (assign == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Assigns.Remove(assign);
        //    db.SaveChanges();

        //    return Ok(assign);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssignExists(int id)
        {
            return db.Assigns.Count(e => e.Id == id) > 0;
        }
    }
}